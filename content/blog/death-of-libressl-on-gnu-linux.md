+++
author = "Raj Porus Hiruthayaraj"
title = "La mort de LibreSSL sur GNU/Linux"
date = 2021-04-21
+++


<div class="figure">
<img src="/images/blog/libressl/libressl.png" alt="drawing" width="800"/>
<p class="caption">Logo de LibreSSL</p>
</div>

## LibreSSL

LibreSSL est un ensemble de chiffrement implémentant les protocoles SSL et TLS, il a été forké à partir d'OpenSSL.
Dans LibreSSL 90 000 lignes de code jugées obsolètes ont été supprimées.
Heartbleed était une attaque lié à OpenSSL et c'est vraiment cet événement qui a engendré la création de LibreSSL,
ce dernier se voulant plus sécurisé.

## Discontinué sous GNU/Linux

Sur GNU/Linux les deux distributions majeurs qui supportaient
LibreSSL ont arrêté de les maintenir.
Gentoo a arrêté de supporter davantage LibreSSL depuis
le 5 janvier 2021 et Void Linux a arrêté en février 2021
juste après.

Avec une grande majorité de programmes ne supportant que
OpenSSL, les développeurs de ces distributions ne
pouvait pas continuer à patcher et offrir un support pour
ce dernier.

## Mon opinion

En ce qui me concerne, je pense tester [OpenBSD](https://www.openbsd.org/) juste pour
cette raison là, car je préférais avoir OpenSSL sur
mon ordinateur portable. En revanche, OpenBSD est un système d'exploitation plus compliqué à installer, car il est très minimal et tellement
sécurisé qu'il ne propose pas de support pour certains
protocoles comme le Bluetooth par exemple.

## Sources
- [Switching back to OpenSSL, Void Linux](https://voidlinux.org/news/2021/02/OpenSSL.html)
- [LibreSSL support discontinued, Gentoo Linux](https://www.gentoo.org/support/news-items/2021-01-05-libressl-support-discontinued.html)
