+++
author = "Raj Porus Hiruthayaraj"
title = "Introduction to Godot part 1, Movements"
date = 2022-03-14
+++


<div class="figure">
<img src="/images/blog/godot/pt1/godot-logo.png" alt="logo" width="800"/>
</div>

## Why am I doing this tutorial ?

I've planned to teach game making to my siblings, but I have no prior experience in that. I intend to log everything I learn in this tutorial so that I can direct them to this resource. Also I am writing this tutorial in english as I recall my notes better like that. (sorry french bros)

## What are the prerequisites for this project ?

You don't have to know anything about game dev, because I don't either. However a basic understanding of programming is required. The language used in this seriesis C#. You don't need to be an expert in it. Check this [website](https://learnxinyminutes.com/docs/csharp/) to learn the basics of the language. Basic concepts are conditions, loops, fucntions, and classes. Once you got that, you can follow these posts without difficulty.

## Get started

Install godot-mono, be sure to install the mono version or else you won't be able to run C# scripts. You can find all the instructions you need for that on Godot's own website.
Now that you have installed godot-mono, create a new project and we are ready to start. Click on new project, then create & edit.

<div class="figure">
<img src="/images/blog/godot/pt1/godot_create_project.png" alt="logo" width="800"/>
</div>

Your project should look like this :

<div class="figure">
<img src="/images/blog/godot/pt1/godot_first_scene.png" alt="logo" width="800"/>
</div>

You are presented with an empty 3D scene. Don't worry about this for now, also keep in mind that our game will be in 2D so we won't use that for now.

The first thing we want to do is to create a folder for our "scenes", you can think of scenes as levels, and anything that is on your screen. Right click on "res://" and click on new folder, name it "Scenes"

<div class="figure">
<img src="/images/blog/godot/pt1/godot new folder.png" alt="logo" width="800"/>
</div>

Now on the top left of your screen press the 2D scene button, Once you have pressed it you will see an unsaved 2D scene like this :

<div class="figure">
<img src="/images/blog/godot/pt1/godot_unsaved_scene.png" alt="logo" width="800"/>
</div>

Press ctrl + s to have your scene, and then select the "Scenes" folder:

<div class="figure">
<img src="/images/blog/godot/pt1/godot_choose_scene_folder.png" alt="logo" width="800"/>
</div>

You can name your scene whatever you like, I just went with the name "Main"

# Chapter 1 : Movements

Now that you have your 2D scene, just add a kinematic body, add a sprite to it, attach a colision body for it, attach a script, make it move, and we are done. Thank you for reading my post, let's see you again in the next part ! :)


Just kidding, I am not going to let you down just yet..

## Adding the KinematicBody2D

Click on the plus icon on top of your 2D Node and you would be presented with a screen where you can choose different components : 

<div class="figure">
<img src="/images/blog/godot/pt1/godot component selection.png" alt="logo" width="800"/>
</div>

and type on the search bar : "KinematicBody2D" then click on it.

In godot you have many components and each serve a specific purpose, you can have static shapes, shapes that are there only to make colisions etc...
The kinematicBody we choose will help us *move*. This will become our player in this tutorial.

Now that you added your KinematicBody2D, you'll see a yellow triangle next to it, it's a warning, you can click on it to display it in details :

<div class="figure">
<img src="/images/blog/godot/pt1/godot kine warning.png" alt="logo" width="800"/>
</div>

You can read on those errors that your KinematicBody has no CollisionShape, it's not a problem for now but just to make the error go away let's do this : Click on your KinematicBody2D and press the plus icon like you used to for the scene, then enter "CollisionShape2D", and now your screen should be like this.

<div class="figure">
<img src="/images/blog/godot/pt1/godot collision shape added.png" alt="logo" width="800"/>
</div>

Now press on the CollisionShape2D and in the menu on the right of your screen you should be able to see the option shape that yields empty : 

<div class="figure">
<img src="/images/blog/godot/pt1/godot collision shape added.png" alt="logo" width="800"/>
</div>

We are going to choose the rectangle shape : then draw your rectangle like this

<div class="figure">
<img src="/images/blog/godot/pt1/godot draw rectangle.png" alt="logo" width="800"/>
</div>

If you hit play right away now this is what you will see :

<div class="figure">
<img src="/images/blog/godot/pt1/godot select current.png" alt="logo" width="800"/>
</div>

Just press on select current for the scene you are working on to be chosen as the main one, once you've done that you can see your game's screen :

<div class="figure">
<img src="/images/blog/godot/pt1/godot empty screen.png" alt="logo" width="800"/>
</div>

We don't see our rectangle collision shape as it's invisble in the game.

As we want to see something in the kinematicBody that we are going to move around, click on the KinematicBody and add the component called "Polygon2D", then you can draw the polygon you want :

<div class="figure">
<img src="/images/blog/godot/pt1/godot draw polygon.png" alt="logo" width="800"/>
</div>

And now if you play the game you would be able to see the shape you've drawn on the screen.
The weird shape I've drawn above will be our player, and the blue rectangle around it is its collision, I made it bigger than the polygon, in a real game it's what you wanna do if you want your players to complain about the hitbox ;)

As in this chapter we will only talk about the movement it's none of our concern...for now.

## Attach your first script

Now we are getting into the good part, the part where you get to *code*.
Just like you did with the folder "Scenes", create a new one called "Scripts".
Once you got your new folder click on KinematicBody2D, then press on this button to attach a script :

<div class="figure">
<img src="/images/blog/godot/pt1/godot attach script button.png" alt="logo" width="800"/>
</div>

then you will be presented with this menu :

<div class="figure">
<img src="/images/blog/godot/pt1/godot attach script menu.png" alt="logo" width="800"/>
</div>

Make sure that you've selected C# as the language, then check the path of your script, you want it to be stored in your "Scripts" folder. Once you verified this, press on create.

<div class="figure">
<img src="/images/blog/godot/pt1/godot kinematic code.png" alt="logo" width="800"/>
</div>

If you can see the screen above and a code similar to that, we are good to go.

The code you see above has two functions, one is called "\_Ready" and the other one "\_Process".
These are functions inside of the class KinematicBody2D, this class inherits from the godot class "Godot.KinematicBody2D".

Inside the Godot's class, we have our two functions, to use them we have to "override" them in OUR class, which is here called KinematicBody2D.
You can uncomment the second function.

Now what do these functions do ? 

The function named Ready, will be launched when your node (in our case the KinematicBody2D of the player) will enter the scene, it would be run usually once, as entering the scene is a one time thing.

The function Process on the other hand will be called multiple times. You must have noticed that the function Process has a parameter called delta.
Delta represents **the time your last frame took to run**, so if you game runs at 60 frames per second, delta would be : 1/60 seconds.
The function Process, will be called for every frame in your game, it's the heartbeat of your game in a way.
If your player is animated, it's this functions's duty to let us know what changes, if your player moves, it also goes through this function, to know if your player has been hit and to lower his health, also called in this function. But for this enumeration, you know that this function might be long, so for the sake of better code clarity, we will put the movements, collision, animations and so on in seperate functions, that will be in turn called inside process, it would make things tidier.

## Key inputs
