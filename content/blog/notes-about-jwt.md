+++
author = "Raj Porus Hiruthayaraj"
title = "Notes à propos de JWT"
date = 2021-08-11
+++

Je partage dans cet articles les notes que j'ai pu prendre en lisant le ["JWT Handbook"](https://legatoplay.github.io/2019/04/17/JWT/jwt-handbook-v0_14_1.pdf).

# Table of Contents

1.  [What is JWT ?](#org97f0620)
    1.  [It&rsquo;s stands for Json Web Token, and is a standard for safely passing claims.](#orgc8daf56)
    2.  [A JSON Web Token looks like this (newlines inserted for readability):](#orga301b5d)
    3.  [it also has a signature to verify its authenticity :](#org778f289)
    4.  [The magic of JWT is standardizing the claims](#orgb9dd34c)
2.  [Problems solved by JWT](#orge0eba31)
3.  [History of JWT](#org7b35352)
4.  [Client-side / Stateless Session](#orgb3ccda5)
5.  [Signature stripping](#org5afb504)
6.  [Cross-Site Request Forgery (CSRF)](#org2094547)
7.  [Cross-Site Scripting (XSS)](#orgc9b9ce2)
8.  [Are client-side session useful ?](#orge748210)
9.  [Federated identify](#org7ead686)
    1.  [Example :](#org68e844f)
10. [Access and Refresh Tokens](#org6e1e798)
    1.  [Access](#org9c2ccf5)
    2.  [Refresh](#org629e3bf)
    3.  [Benefits](#orgba669da)
11. [In detail](#org36f5b8a)
    1.  [Header](#org1415a65)
    2.  [Payload](#org7d48768)
        1.  [The following are all registered claims :](#org92eb3a3)
12. [Public and private claims](#orgd5e5d53)
    1.  [Private claims](#orgb203e11)
    2.  [Public claims](#orge9ce596)
13. [Unsecured JWT](#org8089c10)
14. [Create an unsecured JWT](#org5881d98)
15. [JSON Web Signatures](#org2eeed1f)



<a id="org97f0620"></a>

# What is JWT ?


<a id="orgc8daf56"></a>

## It&rsquo;s stands for Json Web Token, and is a standard for safely passing claims.

-   used in all modern web frameworks (not by default but trough plugins)


<a id="orga301b5d"></a>

## A JSON Web Token looks like this (newlines inserted for readability):

    eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.
    eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.
    TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ

the format is like this :
header.
payload.
signature


<a id="org778f289"></a>

## it also has a signature to verify its authenticity :

    {
        "alg": "HS256",
        "typ": "JWT"
    }
    {
        "sub": "1234567890",
        "name": "John Doe",
        "admin": true
    }


<a id="orgb9dd34c"></a>

## The magic of JWT is standardizing the claims


<a id="orge0eba31"></a>

# Problems solved by JWT

What JWT brings to the table is to be a simple, useful, standard container format.

-   Authentication
-   Authorization
-   Federated identity
-   Client-side sessions (“stateless” sessions)
-   Client-side secrets


<a id="org7b35352"></a>

# History of JWT

The JOSE (JSON Object Signing and Encryption group) was founded in 2011.
Their goal was to standardize :

-   integrity protection
-   encryption
-   keys and algorithms
-   interoperability for the protocols that use JSON


<a id="orgb3ccda5"></a>

# Client-side / Stateless Session

Stateless session are just client side data, take for example the items in the
user&rsquo;s shopping cart.
As these datas are prone to tampering they should be handled with care by the back-end.
There for we can use encryption provided by JWT to encrypt this data, and
prevent it from being read by a third-party program.


<a id="org5afb504"></a>

# Signature stripping

A very common attack on JWT is to remove the signature.
A JWT is composed of signature, header and payload.
These 3 are encoded separately, and there for it&rsquo;s possible to remove the
signature and change the header, making the JWT unsigned.
Sometimes when using JWT library carelessly, it can happen that unsigned JWT are
taken as valid.
In order to solve this we just need to make sure that usigned JWT are not taken
as valid by our program.


<a id="org2094547"></a>

# Cross-Site Request Forgery (CSRF)

This attack consists of tricking the user&rsquo;s browser to send a request to a website :
&ldquo;targer.site.com&rdquo; in order to steal his credentials.
For example a malicious website can have his image tag :

    <!-- This is embedded in another domain's site -->
    <img src="http://target.site.com/add-user?user=name&grant=admin">

If our user is logged in target.site.com, and has used a cookie in it.
Then the browser will send a request as if the user tried to login.
This attack can only work if target.site.com didn&rsquo;t have any CSRF mitigation technique.
JWT, as any other client-side data can be stored as cookies.
In this kind of attack, a JWT with a shorter life span can help.
A very common CSRF mitigation technique is to use special headers to the JWT
only when the request is performed from the right origin.
If JWT are not stored as cookies this attack is not possible however Cross Site
Scripting (XSS) attacks are still possible.


<a id="orgc9b9ce2"></a>

# Cross-Site Scripting (XSS)

This attack is about injecting javascript in trusted websites.
The javascript code and access token and if they are still alive, use it to
access datas that he would otherwise not been able to see.
This attack results from poor input validation, similar to SQL injection.
To prevent this, input data must be sanitized. Making the cookies HttpOnly can
help, but these cookies will still be vulnerable to CSRF attack.


<a id="orge748210"></a>

# Are client-side session useful ?

There are pros and cons to any approach so do some benchmarks to know what
approach is the best for you.


<a id="org7ead686"></a>

# Federated identify

This is about sharing authorization services between unrelated parties.
These two solution are the most common for this :

-   SAML
-   OpenID Connect, layer around oauth2

JWT can be used for that.


<a id="org68e844f"></a>

## Example :

In order for a user to access the ressource behind a server, he makes a
request :

-   User doesn&rsquo;t have the proper credentials and he is redirected to the
    authorization server
-   User is redirected to the identity provider&rsquo;s login screen (example:gmail,twitch,steam)
-   The authorization server uses the credentials from the identity provider to
    access the credentials requested by the ressource server
-   The user, now that he has the correct credentials, he can access the ressource


<a id="org6e1e798"></a>

# Access and Refresh Tokens


<a id="org9c2ccf5"></a>

## Access

Access token are the one that give access to the ressource.
They are short lived and carry additional data in them, such as IP address from
which the request is allowed. The additional data depends on the way that it&rsquo;s implemented.


<a id="org629e3bf"></a>

## Refresh

On the contrary, refresh tokens are long lived. These tokens let the users
request an access token.


<a id="orgba669da"></a>

## Benefits

This separation allows the access token to be easily verified without asking the
authorization server. The signed JWT can be validated by the ressource server on its own.
However the refresh token, has to be sent to the authorization server as it
lasts longer than an access token, and therefore, the call that contains the
refresh token can take longer. The duration is explained the client verification
and other processes that might come to play in order to make sure there is no
refersh token leak. In the even that it might be leaked, it must be blacklisted
by the server.


<a id="org36f5b8a"></a>

# In detail

The encoding is a more browser safe version of base64 where &ldquo;+&rdquo; and &ldquo;/&rdquo; are
replaced by &ldquo;-&rdquo; and &ldquo;\_&rdquo; respectively. This variant is called base64url.
When a JWT token is decoded it looks like this :

-   The JWT:
    eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.
    eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.
    TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ
-   The decoded header:
    
        {
        "alg": "HS256",
        "typ": "JWT"
        }
-   The decoded payload is:
    
        {
        "sub": "1234567890",
        "name": "John Doe",
        "admin": true
        }
-   The secret to verify the signature is &ldquo;secret&rdquo;


<a id="org1415a65"></a>

## Header

-   &ldquo;alg&rdquo; stands for the algorithm used to sign and decrypt
-   &ldquo;typ&rdquo; defines the media type of the JWT itself
-   &ldquo;cty&rdquo; defines the content type, and is equal to JWT if it carries a JWT. It
    should not be set otherwise. This occurs only in nested JWT, which is very
    rare.


<a id="org7d48768"></a>

## Payload

This is where we store all the interesting user&rsquo;s data. Justlike the header this
is also a JSON.


<a id="org92eb3a3"></a>

### The following are all registered claims :

-   &ldquo;iss&rdquo; A case sensitive string that identifies the party that issued the JWT.
    Its usage is application specific.
-   &ldquo;sub&rdquo; identifies the party concerned about the information in the JWT&rsquo;s
    payload. It must be unique in that context.
-   &ldquo;aud&rdquo; this tells us who is allowed to read the JWT
-   &ldquo;exp&rdquo; is the expiration time of the JWT
-   &ldquo;nbf&rdquo; indicates the exact moment of which this JWT was considered valid
-   &ldquo;iat&rdquo; time when the JWT was issued
-   &ldquo;jti&rdquo; unique id to this JWT

As you can see the names should be very small in JWT, its one of the design
requirements


<a id="orgd5e5d53"></a>

# Public and private claims

All claims that are not part of [registered claims](#org92eb3a3) are either a public or a
private claim.


<a id="orgb203e11"></a>

## Private claims

These are defined by the user (consumer or producer) of the JWTs, these claims
are there for a particular use case, and any collision in the naming must be
prevented.


<a id="orge9ce596"></a>

## Public claims

These are either registered with IANA JSON Web Token Claims registry. This is
where user can register their claims and therefore preventing collision or name
using a name that is secure against such collision, for instance, prepending a
namespace to its name.


<a id="org8089c10"></a>

# Unsecured JWT

This JWT with not signature or encryption is often used on the client side. In
real life the unsecured JWTs are very rare. Mostly the data contained here don&rsquo;t
have a lot of importance, if they are being tampered, it won&rsquo;t be a disturbance.


<a id="org5881d98"></a>

# Create an unsecured JWT

To create an unsecured JWT, you just have to encode the header and body in
base64url, separately. And once you have them both encrypted, you concatenate
them using a &ldquo;.&rdquo; (dot).


<a id="org2eeed1f"></a>

# JSON Web Signatures

The purpose of this signature is to verify the authenticity of the JWT. In this
case authenticity means that the data contained must not be tampered with.
Therefore the other party can and has to do a signature check in order to verify
the authenticity of the data. The signature allows the data to be unreadable
from a third party.
A JWT may be considered valid even without a signature, a JWT with a signature
can also be considered invalid, for instance if the &ldquo;exp&rdquo; field claims that the
token has expired.
