+++
author = "Raj Porus Hiruthayaraj"
date = 2021-05-23
+++

# Sommaire

1.  [Learnxinyminutes](#learnxinyminutes)
2.  [Xah Lee](#xahlee)
3.  [InstallGentooWiki](#installgentoo)
4.  [Luke Smith](#lukesmith)
5.  [Les meilleurs Wiki de GNU/Linux](#linux)
6.  [Liens temporaires](#temp)

Dans cette section de mon site, j'ai regroupé un ensemble d'URL qui peut être
utile pour les développeurs ou toutes autres personnes s'intéressant à la
technologie et l'informatique. Quelques années, auparavant, on avait pour habitude de chercher
nous-même des sites pour répondre au problème que nous pouvions rencontrer. Mais
en ce moment, notre usage d'Internet selon moi s'est limité à quelques grands
sites. J'espère alors vous faire découvrir de nouveaux sites dont il est très
peu probable que vous connaissiez l'existence. Si vous avez des sites à me
suggérer, n'hésitez pas à m'en faire part via un mail : 
[raj.porus.hiruthayaraj@gmail.com](mailto:raj.porus.hiruthayaraj@gmail.com).


<a id="learnxinyminutes"></a>

# Learnxinyminutes

Ce site vous permet d'acquérir les bases sur un langage très rapidement, et le fait de
lire le code dans le langage que vous voulez apprendre représente pour moi une méthode
bien plus efficace que de visionner des didacticiels concernant ce langage.

-   [learnxinyminutes.com](https://learnxinyminutes.com/)


<a id="xahlee"></a>

# Xah Lee

Ce blog contient un océan de connaissances sur l'informatique, Xah Lee possède également une chaîne Youtube. J'ai tout d'abord découvert son contenu lorsque j'avais commencé à utiliser
l'éditeur de code Emacs. Vous pouvez retrouver des articles parlant de langages informatiques,
Emacs, les claviers et les différentes dispositions des touches d'un clavier.
Je vous recommande vivement d'aller faire un tour sur son site, et de vous abonner à son flux Atom pour ne pas rater ses nouveaux contenus.

-   [xahlee.info](http://xahlee.info/index.html)
-   [La chaîne Youtube de Xah Lee](https://www.youtube.com/channel/UCXEJNKH9I4xsoyUNN3IL96A)
-   [Atom Webfeed de Xah Lee](http://xahlee.info/js/blog.xml)


<a id="installgentoo"></a>

# InstallGentooWiki

Ce wiki est maintenu par le bord technologique de 4chan : [4channel.org/g/](https://boards.4channel.org/g/).
4chan a une mauvaise réputation de manière générale par son manque ou absence de modération en
partie, et l'anonymat des utilisateurs qui peuvent donner lieu à des comportement qui ne
seraient pas là si leurs noms était public. En revanche, le bord technologique n'est pas
mauvais et il peut parfois être assez utile. Le meilleur terrain d'entente entre profiter des connaissance du bord technologique tout en évitant de faire face à du contenu parfois douteux,
il est préférable alors d'utiliser ce wiki. Il faut savoir passer outre le langage taquin vis-à-vis des néophytes cependant, et comprendre le jargon de ce bord.

-   [wiki.installgentoo.com](https://wiki.installgentoo.com/index.php/Main_Page)


<a id="lukesmith"></a>

# Luke Smith

Luke Smith ne parle pas que d'informatique, il parle de crypto, économie, de linguistiques (qu'il a étudié), de GNU/Linux, politique, religion, science, et philosophie. Vous pouvez aller
voir ses vidéos qui traitent de divers sujets, mais si vous êtes là pour l'informatique, Luke Smith a de très bonne vidéo où il parle de GNU/Linux, Vim et de minimalisme informatique.
J'aime en particulier ses tutoriels sur LaTeX, et c'est comme ça que j'ai découvert sa chaîne.
Ses vidéos sont disponible sur Odysee et Youtube, il a aussi une instance de PeerTube, mais vous ne pouvez que retrouver ses vidéos les plus récentes sur cette dernière.

-   [lukesmith.xyz](https://lukesmith.xyz/)
-   [Flux RSS de Luke Smith](https://lukesmith.xyz/rss.xml)
-   [Chaîne Odysee de Luke Smith](https://odysee.com/@Luke:7)
-   [L'instance PeerTube de Luke Smith](https://videos.lukesmith.xyz/)
-   [La chaîne Youtube de Luke Smith](https://www.youtube.com/channel/UC2eYFnH61tmytImy1mTYvhA)


<a id="linux"></a>

# Les meilleurs Wiki de GNU/Linux

Voici trois site que j'utilise pour trouver rapidement la solution à un problème. Généralement la commande [man](https://wiki.archlinux.org/title/Man_page) est suffisante dans 80% des cas.
Si le souci persiste, je me rends sur le wiki d'Arch Linux qui est le site ayant le plus de contenu de cette liste. Même si vous n'utilisez pas Arch
ce site est toujours très utile pour vous, il faut juste tenir compte que le nom des programmes sont similaire, mais pas identique, les fichiers ne sont pas au même endroit
et que la commande pour installer et mettre à jour vos programmes sont différentes. Si le wiki d'Arch ne résous pas votre problème (ce qui est improbable) vous pouvez faire un
tour sur le wiki de Gentoo et ensuite le wiki de Void. Gentoo étant la distribution la plus élitiste des trois, ce dernier pourra répondre à vos problèmes les moins répandus.
Sur Gentoo on compile tous nos programmes nous-même, vous trouverez alors à coup sûr toutes les dépendances d'un programme.

-   [https://wiki.archlinux.org/](https://wiki.archlinux.org/)
-   [https://wiki.gentoo.org/](https://wiki.gentoo.org/)
-   [https://wiki.voidlinux.org](https://wiki.voidlinux.org/)



<a id="temp"></a>

# Liens temporaire

Faites pas attention, ces liens sont pour ma soeur ^^

-  [Lien pour la présentation de Nighthawks (Melkees)](https://docs.google.com/presentation/d/e/2PACX-1vTql8RhnHzSjv7i8xM6cOE-aA-7Y91NSfnBMam_xvsO6npnsq5p-yqCo5eS5UgsbUsNSGzMlkSGFRvI/pub?start=false&loop=false&delayms=60000)
-  [Lien pour la présentation de stage (Melkees)](https://docs.google.com/presentation/d/e/2PACX-1vS0Cn0-q7YrutkXxWi1UjySBmBvryibHmnZRDclwxley_0oWkheATH06C9Zs_nvenvw7L_Alv3F20xB/pub?start=false&loop=false&delayms=60000&slide=id.g112c9754939_0_711)
-  [Lien pour la présentation de français (Melkees)](https://docs.google.com/presentation/d/e/2PACX-1vQoYOZiVmNv91CgHBJqyc-FE0vnhR0vihsrAAsGO6Fo9Ow4-LgpslEgq33YQOmCqXcXqLBEej9aJZKL/pub?start=false&loop=false&delayms=60000)
-  [Lien pour la présentation d'Opal (Melkees)](https://docs.google.com/presentation/d/e/2PACX-1vQyXRPN9hoqXbMwBHKP5yNuHzWXOuK77czNcyADV3vAban3f-ugXb2gIDozVbSSxEAZa5XaMKw0WOLr/pub?start=false&loop=false&delayms=3000)
