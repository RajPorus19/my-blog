# Live streaming test


<!-- CSS  -->
 <link href="https://vjs.zencdn.net/7.2.3/video-js.css" rel="stylesheet">


<!-- HTML -->


<!-- JS code -->
<!-- If you'd like to support IE8 (for Video.js versions prior to v7) -->

<script src="https://vjs.zencdn.net/7.11.4/video.min.js"></script>
<script src="https://vjs.zencdn.net/7.2.3/video.js"></script>
<script src="https://cdn.jsdelivr.net/npm/videojs-contrib-hls@5.15.0/es5/videojs-contrib-hls.min.js"></script>

<video id='hls-example'  class="video-js vjs-default-skin" width="400" height="300" controls>
<source type="application/x-mpegURL" src="https://stream.porus.dev/hls/show.m3u8">
</video>

<script>
var player = videojs('hls-example');
player.play();
</script>
